// console.log("Hello World!");

// Mock Database
let posts = [];

// Posts ID
let count = 1;


// Reactive DOM with JSON (CRUD Operation)

// ADD POST DATA
// This will trigger an event that will add a new post in our mock database upon clicking the "Create" button.

document.querySelector("#form-add-post").addEventListener("submit", (e) =>{

	// Prevents the page from reloading. Also prevents the default behavior of our event.
	e.preventDefault();

	posts.push({
		// "id" property will be used for the unique ID of each posts.
		id: count,
		// "title" and "body" values will come from the "form-add-post" input elements.
		title : document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});
	// count will increment everytime a new movie is posted.
	count++

	console.log(posts);
	alert("Movie Successfully Added!");

	showPosts();
});

// Retrieve post
const showPosts = () => {

	let postEntries = "" ;

	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}"> 
				<h3 id="post-title-${post.id}">${post.title} </h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button id="edit-btn"onclick="editPost(${post.id})">Edit </button>
				<button id="dlt-btn"onclick="deletePost(${post.id})">Delete</button>
			</div>

		`
	})

	console.log(postEntries);

	document.querySelector("#div-post-entries").innerHTML = postEntries
};

// EDIT POST DATA / Edit button

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value= id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
};

// Update Post
document.querySelector("#form-edit-post").addEventListener("submit",(e)=>{
	e.preventDefault();

	for(let i = 0; i < posts.length; i++){

		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value

			showPosts(posts);
			alert("Successfully updated!");
			break;
		}
	}
});


// Delete Post

const deletePost = (id) =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	if(title.id === posts.id) {
		document.querySelector(`#post-title-${id}`).remove();
		document.querySelector(`#post-body-${id}`).remove();
		document.querySelector(`#edit-btn`).remove();
		document.querySelector(`#dlt-btn`).remove();
	}
	alert("Deleted Successfully!")



	
console.log(posts)
};






